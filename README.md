## Adversarial Generative (AG) Framework

This is an Adversarial Generative (AG) Framework for text generation.

### Details

```Components
It consists of:
1) An encoder-decoder (generative) framework
2) A discriminator (adversarial) framework
```
