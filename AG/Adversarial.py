# -*- coding: utf-8 -*-

import random

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch import optim

import AG.Models as m
import opts

is_adv = opts.options.adv
BATCH = opts.options.batch
use_cuda = torch.cuda.is_available()

def updateD(x_label,x_output,z_label,z_output,adv_criterion,D,lr=0.001):
    # Update discriminator, k = 1
    x_loss = adv_criterion(x_label,x_output,'real')
    z_loss = adv_criterion(z_label,z_output,'fake')
    for param in D.parameters():
        param.data -= lr * (x_loss.data+z_loss.data)
    return x_loss+z_loss

def updateG(z_label,z_output,adv_criterion,encoder,decoder,lr=0.001):
    # Update generator
    loss = adv_criterion(z_label,z_output,'real')
    for param in encoder.parameters():
        param.data -= lr * loss.data
    for param in decoder.parameters():
        param.data -= lr * loss.data
    return loss

def encode(io, input_variable, encoder, max_length):

    input_length = input_variable.size()[0]
    batch = min(input_variable.size()[1],BATCH)

    # Padding
    encoder_outputs = Variable(torch.zeros(max_length, encoder.hidden_size))
    encoder_outputs = encoder_outputs.cuda() if use_cuda else encoder_outputs

    # Encoding
    encoder_hidden = encoder.initHidden()

    # encoder_output : [sent_len,batch,hidden_size]
    # encoder_hidden : [1,batch,hidden_size]
    encoder_output, encoder_hidden = encoder(input_variable,encoder_hidden,batch)

    # Decoding
    decoder_input = Variable(torch.LongTensor([[io.SOS_token]*batch]))
    decoder_input = decoder_input.cuda() if use_cuda else decoder_input
    decoder_hidden = encoder_hidden

    return decoder_input, decoder_hidden, encoder_output, encoder_outputs


def decode(io, decoder, criterion, target_variable, decoder_input, 
           decoder_hidden, encoder_output, encoder_outputs):

    teacher_forcing_ratio = 0.5
    target_length = target_variable.size()[0]

    use_teacher_forcing = True if random.random() < teacher_forcing_ratio else False
    loss = 0

    output_list = []

    if use_teacher_forcing and not(is_adv):
        # Teacher forcing: Feed the target as the next input
        for di in range(target_length):
            decoder_output, decoder_hidden = decoder(
                decoder_input, decoder_hidden, encoder_output, encoder_outputs)
            loss += criterion(decoder_output, target_variable[di])
            decoder_input = target_variable[di]  # Teacher forcing

    else:
        # Without teacher forcing: use its own predictions as the next input
        for di in range(target_length):
            decoder_output, decoder_hidden = decoder( # decoder_output : [batch, vocab_size]
                decoder_input, decoder_hidden, encoder_output, encoder_outputs)
            
            topv, topi = decoder_output.data.topk(1,1) # topi : [batch, 1]
            decoder_input = Variable(topi)
            decoder_input = decoder_input.cuda() if use_cuda else decoder_input
            output_list += [topi]

    # [sent_len,batch]
    output = Variable(torch.cat(output_list,dim=1)).transpose(0,1) 
    label = target_variable

    return loss, target_length, output, label

def indices2emb(decoder,indices):
    # Encoding
    token_emb = decoder.embedding(indices)
    return token_emb

def generate(io,input_variable,target_variable,encoder,decoder,criterion,max_length):
    # Encoder networks
    decoder_input, decoder_hidden, encoder_output, encoder_outputs = \
                            encode(io, input_variable, encoder, max_length)
    # Decoder networks
    loss, target_length, output, label = \
            decode(io, decoder, criterion, target_variable, decoder_input, \
                       decoder_hidden, encoder_output, encoder_outputs)
    return loss, target_length, output, label




