# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

use_cuda = torch.cuda.is_available()


def buildSeqDiscriminator(sent_len, batch, dim, 
               out_dim, num_layers, global_dim):
    return SeqDiscriminator(sent_len, batch, dim, 
                      out_dim, num_layers, global_dim)

def buildSTE(dim):
    return STE(dim)


class CNNDiscriminator(nn.Module):
    r"""
    Discriminator built on Convolutional Neural Networks (CNN).
    It takes variable size dimensions with fixed-size batch.
    
    Returns:
        output: batch size probabilities.

    """
    def __init__(self, batch, output_size, hidden_size=200, max_len=200):
        super(CNNDiscriminator, self).__init__()

        # Define dimensions
        fc1_num = int(hidden_size/2)
        fc2_num = int(hidden_size/4)

        # Define layers
        self.embedding = nn.Embedding(output_size, hidden_size)
        self.conv = nn.Conv1d(max_len, hidden_size, kernel_size=2, padding=2)
        self.fc1 = nn.Linear(hidden_size, fc1_num)
        self.fc2 = nn.Linear(fc1_num, fc2_num)
        self.fc3 = nn.Linear(fc2_num, batch)

        self.nonlinear = nn.Sigmoid()
        
    def forward(self, x):
        x = self.embedding(x)
        out = self.conv(x.transpose(0,1))
        out = out.transpose(1,2)
        out = self.fc1(out)
        out = self.fc2(out)
        out = self.fc3(out)
        out = self.nonlinear(out)
        return out

class PositionalEncoding(nn.Module):
    r""" 
    Credits to https://github.com/OpenNMT/OpenNMT-py/blob/master/onmt/modules/Embeddings.py
    
    """
    def __init__(self, dropout, dim, max_len=5000):
        pe = torch.arange(0, max_len).unsqueeze(1).expand(max_len, dim)
        div_term = 1 / torch.pow(10000, torch.arange(0, dim * 2, 2) / dim)
        pe = pe * div_term.expand_as(pe)
        pe[:, 0::2] = torch.sin(pe[:, 0::2])
        pe[:, 1::2] = torch.cos(pe[:, 1::2])
        pe = pe.unsqueeze(1)
        super(PositionalEncoding, self).__init__()
        self.register_buffer('pe', pe)
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, emb):
        emb = emb + Variable(self.pe[:emb.size(0), :1, :emb.size(2)]
                             .expand_as(emb), requires_grad=False)
        emb = self.dropout(emb)
        return emb

class Embeddings(nn.Module):
    r"""
    This is the customized embedding layer. 
    It includes:
        1) positional encoding
        2) MLP

    output : [seq_len, batch, hidden_size]

    """
    def __init__(self, input_size, hidden_size):
        super(Embeddings, self).__init__()

        self.input_size = input_size
        self.hidden_size = hidden_size
        self.embedding = nn.Embedding(input_size, hidden_size)
        self.posEncoder = PositionalEncoding(dropout=0.1,dim=hidden_size)
        self.mlp = nn.Linear(hidden_size, hidden_size)

    def forward(self, input):
        # input : [sent_len,batch]
        embedded = self.embedding(input)
        embedded = self.posEncoder(embedded)
        output = self.mlp(embedded)
        return output

class EncoderRNN(nn.Module):
    r"""
    This is an encoder which takes as input indices sequences to 
    return output vector and last hidden state.

    """

    def __init__(self, input_size, hidden_size, n_layers=1, batch=32, num_directions=2):
        super(EncoderRNN, self).__init__()

        self.n_layers = n_layers
        self.hidden_size = hidden_size
        self.batch = batch
        self.num_directions = num_directions

        self.embedding = Embeddings(input_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size, num_layers=self.n_layers)

    def forward(self, input, hidden, batch):
        r"""
        Returns:
            output : [seq_len, batch, hidden_size * num_directions]
            hidden : [num_layers, batch, hidden_size]
        """
        hidden = hidden.repeat(1,input.size()[1],1) # [num_layers, batch, hidden_size]
        output = self.embedding(input) # [seq_len, batch, input_size]
        output, hidden = self.gru(output, hidden) 
        return output, hidden

    def initHidden(self):
        r""" 
        Return:
            result : (num_layers * num_directions, batch, hidden_size) 
        """
        result = Variable(torch.zeros(self.n_layers, 1, self.hidden_size))
        if use_cuda:
            return result.cuda()
        else:
            return result

class DecoderRNN(nn.Module):
    r"""
    Decoder that takes as input 

    """
    def __init__(self, hidden_size, output_size, n_layers=1, 
                dropout_p=0.1, max_length=10):
        super(DecoderRNN, self).__init__()
        self.n_layers = n_layers
        self.hidden_size = hidden_size
        self.max_length = max_length

        self.embedding = nn.Embedding(output_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size, num_layers=self.n_layers)
        self.out = nn.Linear(hidden_size, output_size)
        self.softmax = nn.LogSoftmax()

    def forward(self, input, hidden, encoder_output, encoder_outputs):
        output = self.embedding(input)
        output, hidden = self.gru(output, hidden)
        output = self.softmax(self.out(output[0])) 
        # output : [batch, output_size]
        return output, hidden

    def initHidden(self):
        result = Variable(torch.zeros(self.n_layers, 1, self.hidden_size))
        if use_cuda:
            return result.cuda()
        else:
            return result

class AttnDecoderRNN(nn.Module):
    r"""
    This is decoder with attention mechanism.

    """
    def __init__(self, hidden_size, output_size, n_layers=1, 
                dropout_p=0.1, max_length=10):
        super(AttnDecoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.n_layers = n_layers
        self.dropout_p = dropout_p
        self.max_length = max_length

        self.embedding = nn.Embedding(self.output_size, self.hidden_size)
        self.attn = nn.Linear(self.hidden_size * 2, self.max_length)
        self.attn_combine = nn.Linear(self.hidden_size * 2, self.hidden_size)
        self.dropout = nn.Dropout(self.dropout_p)
        self.gru = nn.GRU(self.hidden_size, self.hidden_size)
        self.out = nn.Linear(self.hidden_size, self.output_size)

    def forward(self, input, hidden, encoder_output, encoder_outputs):
        embedded = self.embedding(input)
        embedded = self.dropout(embedded)

        attn_weights = F.softmax(
            self.attn(torch.cat((embedded[0], hidden[0]), 1)))
        attn_applied = torch.bmm(attn_weights.unsqueeze(0),
                                 encoder_outputs.unsqueeze(0))

        output = torch.cat((embedded[0], attn_applied[0]), 1)
        output = self.attn_combine(output).unsqueeze(0)

        for i in range(self.n_layers):
            output = F.relu(output)
            output, hidden = self.gru(output, hidden)

        output = F.log_softmax(self.out(output[0]))
        return output, hidden, attn_weights

    def initHidden(self):
        result = Variable(torch.zeros(1, 1, self.hidden_size))
        if use_cuda:
            return result.cuda()
        else:
            return result

class STE(nn.Module):
    r""" Generator in GAN 
    
    State Transition Estimator with 
            1) Multi-layer perceptron

    Args:
        dim (int): number of features.
    """
    def __init__(self, dim):
        super(STE, self).__init__() 

        # Define dimensions
        mlp_1 = 400
        mlp_2 = 200
        mlp_3 = 100
        mlp_4 = 50
        mlp_5 = 25 
        scalar_dim = 1

        # MLP
        self.mlp =  nn.Sequential(
                      nn.Linear(dim,mlp_1),
                      nn.ReLU(),
                      nn.Linear(mlp_1,mlp_2),
                      nn.ReLU(),
                      nn.Linear(mlp_2,mlp_3),
                      nn.ReLU(),
                      nn.Linear(mlp_3,mlp_4),
                      nn.ReLU(),
                      nn.Linear(mlp_4,mlp_5),
                      nn.ReLU(),
                      nn.Linear(mlp_5,scalar_dim),
                      nn.Sigmoid()
                    )

    def forward(self,feats):
        score = self.mlp(feats)
        return score


class SeqDiscriminator(nn.Module):
    r""" Discriminator in GAN 
    
    Sequence state encoder with 
            1) Self-attention 
            2) Positional encodings

    Args:
        sent_len (int): Length of sentences.
        batch (int): size of batch.
        dim (int): number of dimensions.
        out_dim (int): number of out dimensions.
        num_layers (int): number of stacked RNN layers.
    """
    def __init__(self, sent_len, batch, dim, out_dim=200, 
                        num_layers=1, global_dim=1):
        super(SeqDiscriminator, self).__init__()

        self.sent_len = sent_len
        self.batch = batch
        self.dim = dim
        self.out_dim = out_dim
        self.num_layers = num_layers
        self.global_dim = global_dim

        self.embedding = Embeddings(dim, out_dim)

        # Define layers' dimensions
        contextVec_dim = out_dim*2
        mlp_1 = 200
        mlp_2 = 100
        output_dim = 1

        # Define context vector
        self.attVec = torch.autograd.Variable(torch.randn(1,contextVec_dim), requires_grad=True)

        self.rnn = nn.GRU(dim, 
                          out_dim, 
                          num_layers, 
                          bidirectional=True)
        
        # MLP
        self.mlp = nn.Sequential(
                  nn.Linear(sent_len,mlp_1),
                  nn.ReLU(),
                  nn.Linear(mlp_1,mlp_2),
                  nn.ReLU(),
                  nn.Linear(mlp_2,output_dim),
                  nn.Sigmoid()
                )

    def batchNorm(self,e):
        # Regularizations
        norm = nn.BatchNorm1d(self.batch)
        return norm(e)

    def setBatch(self,batch):
        """ Deal with variable size inputs """
        self.batch = batch
        
    def self_attend(self,outputs):
        
        hs = torch.transpose(outputs,0,1) # [batch, sent_len, dim]

        attVec = self.attVec.repeat(self.batch,self.sent_len,1) # [batch, sent_len, dim]
        attVec = torch.transpose(attVec,2,1) # [batch, dim, sent_len]

        association = torch.bmm(hs,attVec) # [batch, sent_len, dim] * [batch, dim, sent_len]
        alpha = F.softmax(association) # [batch, sent_len, sent_len]
        output = torch.sum(alpha,2) # [batch, sent_len]
        self.sentAtts = output # Save for attention analysis

        return output

    def forward(self,input):
        r"""
        Args:
            input: sequence of indices.

        Returns:
            output: probability of a distribution.
        """

        # input : [sent_len,1]
        # emb(input : [sent_len,1,dim])

        # Embedding
        outputSeqs = self.embedding(input).view(1, 1, -1)

        # Sequential forward
        outputs, hn = self.rnn(self.batchNorm(outputSeqs))

        # Self-attention
        contextVec = self.self_attend(outputs) # [batch, dim]

        # Concatenate 
        #finalVec = torch.cat([domain_vector,contextVec],1) # [batch, dim]

        # MLP layers
        output = self.mlp(contextVec) # [batch, 1]

        return output
        






