# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function, division
import random
import os

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch import optim

import IO
import opts
import AG.Models as m
import AG.Loss as l
import AG.Adversarial as ag
import report

use_cuda = torch.cuda.is_available()

# Get options
TOTAL_EPOCHS = opts.options.epochs
BATCH = opts.options.batch
MAX_LEN = opts.options.max_len
RNN_NUM = opts.options.rnnlayers
PRINT_EVERY = opts.options.print_every
LR = opts.options.lr
is_adv = opts.options.adv

def main():

    io = IO.IO()

    io.prepareData('Question', 'Answer')

    # Generator
    hidden_size = 256
    encoder = m.EncoderRNN(io.input_source.n_words, hidden_size)
    decoder = m.DecoderRNN(hidden_size, io.output_target.n_words,
                            1, dropout_p=0.001, max_length=MAX_LEN)

    # Discriminator
    batch = 1
    D = m.CNNDiscriminator(BATCH, io.output_target.n_words)
    
    if use_cuda:
        encoder = encoder.cuda()
        decoder = decoder.cuda()

    trainer(io, encoder, decoder, D, total_epochs=TOTAL_EPOCHS)


def trainer(io, encoder, decoder, D, total_epochs=1000):
    """  
    This function defines the training procedure.
    
    Args: 
        io(IO): IO object.
        encoder(EncoderRNN): encoder object.
        decoder(AttnDecoderRNN): decoder object.
        total_epochs(int): number of epochs.

    Returns:
        None

    """ 

    if is_adv:
        D_optimizer = optim.Adam(D.parameters(), lr=LR)
        G_optimizer = optim.Adam(list(encoder.parameters())+list(decoder.parameters()), lr=LR)
        optimizers = (D_optimizer,G_optimizer)
    else:
        encoder_optimizer = optim.Adam(encoder.parameters(), lr=LR)
        decoder_optimizer = optim.Adam(decoder.parameters(), lr=LR)
        optimizers = (encoder_optimizer,decoder_optimizer)

    criterion = nn.NLLLoss()
    adv_criterion = l.adv_criterion(D)

    from progressbar import ProgressBar, Bar, Percentage, ETA, ReverseBar

    start_epoch = 1

    data_size = max(int(io.datasize/BATCH)+1,2)
    print('Total data points {}.'.format(io.datasize))
    print('Testing on batch size {}.'.format(BATCH))

    for epoch in range(start_epoch,total_epochs+1):

        print('Epoch {}/{}'.format(epoch,total_epochs))

        fake_training_pairs = [io.generateZ() for i in range(data_size)]
        training_pairs = [io.pair2Variables(random.choice(io.pairs))
                                        for i in range(data_size)]

        b = IO.batch(io,batch=BATCH,ratio=1)

        start = time.time()
        plot_losses = []
        print_loss_total = 0  
        plot_loss_total = 0  
        if is_adv:
            D_loss = 0
            G_loss = 0

        pbar = ProgressBar(widgets=[Percentage(), Bar('>'), ETA(), ReverseBar('<')], 
                           maxval=data_size).start()
        progress = 0
        while b.hasNext():  
            loss = step(io, b.x_source, b.x_target, b.z_source, 
                        b.z_target, encoder, decoder, D, optimizers, 
                        criterion, adv_criterion)

            if is_adv:
                D_loss += loss[0]
                G_loss += loss[1]
            else:
                print_loss_total += loss
                plot_loss_total += loss

            progress += 1
            pbar.update(progress)

        pbar.finish()

        if is_adv:
            D_loss_avg = D_loss / data_size
            G_loss_avg = G_loss / data_size            
            print( str('%s (%d %d%%) D-loss %.4f G-loss %.4f' % 
                                         (report.timeSince(start, progress / data_size),
                                         progress, progress / data_size * 100, D_loss_avg, G_loss_avg))
                                         .center(os.get_terminal_size().columns) )
            # Save Model 
            if abs(D_loss_avg-G_loss_avg)<0.001 and D_loss_avg<0.1:
                checkpoint = {
                    'opt': opts.options,
                    'epoch': epoch,
                    'io': io,
                    'D': D,
                    'encoder': encoder,
                    'decoder': decoder,
                    'D_optimizer': D_optimizer,
                    'G_optimizer': G_optimizer
                }
                torch.save(checkpoint,open(opts.options.model+'/model_'+str(epoch),'wb'))
                return None  
        else:
            # Report
            print_loss_avg = print_loss_total / data_size
            print( str('%s (%d %d%%) Gen %.4f' % (report.timeSince(start, progress / data_size),
                                         progress, progress / data_size * 100, print_loss_avg))
                                         .center(os.get_terminal_size().columns) )

            plot_loss_avg = plot_loss_total / data_size 
            plot_losses.append(plot_loss_avg)  

    report.showPlot(plot_losses) 

def step(io, input_variable, target_variable, fake_input_variable, fake_target_variable, 
        encoder, decoder, D, optimizers, criterion, adv_criterion, max_length=MAX_LEN):
    """
    This function steps through the entire encoder-decoder model.

    Returns:
        ave_loss(int): average loss over sequence length.
    """

    if is_adv:

        D_optimizer,G_optimizer = optimizers

        D_optimizer.zero_grad()
        G_optimizer.zero_grad()

        # z generation
        _, _, z_output, z_label = \
            ag.generate(io,fake_input_variable,fake_target_variable,
                        encoder,decoder,criterion,max_length)

        # x generation
        _, _, x_output, x_label = \
            ag.generate(io,input_variable,target_variable,
                        encoder,decoder,criterion,max_length)

        # Update D 
        d_loss = ag.updateD(x_label,x_output,z_label,z_output,adv_criterion,
                            D,lr=D_optimizer.param_groups[-1]['lr']) # k = 1
        # Step D optimizer
        D_optimizer.step() 

        # Update G 
        g_loss = ag.updateG(z_label,z_output,adv_criterion,encoder,decoder,
                            lr=G_optimizer.param_groups[-1]['lr'])
        # Step G optimizer
        G_optimizer.step()

        # Report loss
        ave_loss = (d_loss.data[0],g_loss.data[0])

    else:

        encoder_optimizer,decoder_optimizer = optimizers

        encoder_optimizer.zero_grad()
        decoder_optimizer.zero_grad()

        # Generation
        loss, target_length, _ = \
                ag.generate(io,input_variable,target_variable,
                            encoder,decoder,criterion,max_length)

        loss.backward()

        ave_loss = loss.data[0] / target_length

        # Step optimizer / change lr
        encoder_optimizer.step()
        decoder_optimizer.step()
    
    return ave_loss


if __name__ == "__main__":
    import time
    start = time.time() 
    main()
    print("Time taken {}".format(time.time()-start))
