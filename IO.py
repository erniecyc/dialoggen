# -*- coding: utf-8 -*-

import unicodedata
from io import open
import unicodedata
import string
import re

import torch
from torch.autograd import Variable as Variable
import numpy as np

import opts

dataPath = opts.options.data
max_len = opts.options.max_len
use_cuda = torch.cuda.is_available()

class batch:
    """
    This class prepares batch.

    """
    def __init__(self,io,batch=32,ratio=0.01,max_length=150):
        self.iter = self.iterbatch(io,batch,ratio)
        self.done = False
        self.max_length = max_length
        self.x_source = None
        self.x_target = None 
        self.z_source = None 
        self.z_target = None

    def hasNext(self):
        try:
            # Load data
            self.x_source,self.x_target,self.z_source,self.z_target \
                = self.iter.__next__()
        except StopIteration as e:
            self.done = True
        return not(self.done)

    def pad(self,l):

        for e in l:
            # Padding
            e_ = torch.zeros(self.max_length, encoder.hidden_size)
            e += e_ 

    def iterbatch(self,io,batch,ratio):
        """
        This class make batches of data available.

        """
        import random
        data_size = int(io.datasize*ratio)
        fake_training_pairs = [io.generateZ() for i in range(data_size)]
        training_pairs = [io.pair2Variables(random.choice(io.pairs))
                                        for i in range(data_size)]

        # Getting...
        # input_variable, target_variable, 
        # fake_input_variable, fake_target_variable

        for ndx in range(0, data_size, batch):

            Xs = training_pairs[ndx:min(ndx+batch, data_size)]
            Zs = fake_training_pairs[ndx:min(ndx+batch, data_size)]

            x_source, z_source = [], []
            x_target, z_target = [], []

            for i in range(len(Xs)):
                x_source.append(Xs[i][0])
                x_target.append(Xs[i][1])
                z_source.append(Zs[i][0])
                z_target.append(Zs[i][1])

            yield torch.cat(x_source,1), torch.cat(x_target,1), \
                  torch.cat(z_source,1), torch.cat(z_target,1)

class IO:
    """
    This class prepares data.

    """
    def __init__(self):
        self.input_source = None 
        self.output_target = None
        self.discrim_vocab = None
        self.pairs = None
        self.SOS_token = 0
        self.EOS_token = 1
        self.UNK = 'UNK'
        self.datasize = 0

    def prepareData(self, source, target, reverse=False):

        input_source, output_target, pairs = self.readData(source, target, reverse)

        print("Read %s sentence pairs" % len(pairs))
        print("Counting words...")
        for pair in pairs:
            input_source.addSentence(pair[0])
            output_target.addSentence(pair[1])

        # Gather information for discriminator
        discrim_vocab = Vocab(source+'-'+target)
        for pair in pairs:
            discrim_vocab.addSentence(pair[0])
            discrim_vocab.addSentence(pair[1])

        print("Vocab sizes:",input_source.name, 
                input_source.n_words, output_target.name, output_target.n_words)

        print("Total vocab size:",discrim_vocab.n_words)

        self.input_source = input_source
        self.output_target = output_target
        self.discrim_vocab = discrim_vocab
        self.pairs = pairs
        self.datasize = len(self.pairs)

    def generateZ(self):
        import random
        source = np.random.randint(0,self.input_source.n_words, size=(max_len,1)) 
        source = Variable(torch.LongTensor(source))
        target = np.random.randint(0,self.output_target.n_words, size=(max_len,1))
        target = Variable(torch.LongTensor(target))        
        return (source,target)

    def sent2Variable(self,vocab,sentence):
        indices = [vocab.word2index[word] if word in vocab.word2index.keys()
                    else vocab.word2index[self.UNK] for word in sentence ]
        result = Variable(torch.LongTensor(indices+np.zeros(max_len-len(indices))).view(-1, 1))
        if use_cuda:
            return result.cuda()
        else:
            return result
        return result

    def sentence2Indices(self, vocab, sentence):
        indices = [vocab.word2index[word] if word in vocab.word2index.keys() 
                    else vocab.word2index[self.UNK] for word in sentence.split(' ')]
        return indices+[1]*(max_len-len(indices)-1)

    def sentence2Variable(self, vocab, sentence):
        indices = self.sentence2Indices(vocab, sentence)
        indices.append(self.EOS_token)
        result = Variable(torch.LongTensor(indices).view(-1, 1))
        if use_cuda:
            return result.cuda()
        else:
            return result

    def pair2Variables(self, pair):
        source = self.sentence2Variable(self.input_source, pair[0])
        target = self.sentence2Variable(self.output_target, pair[1])
        return (source, target)

    def convert2turns(self,lines):
        return [ '\t'.join( [ l[1] for l in lines[i:i+2] ] ) for i in range(len(lines)) ]

    def readMovies(self,dataPath):
        """ Read movies transcripts """

        # Read the file 
        lines = []
        for line in open(dataPath,'rb').readlines():
            try:
                fields = line.decode('utf-8').split('+++$+++')
                name = fields[3]
                sent = fields[-1].replace('\n','').strip()
                lines.append( (name,sent) )
            except Exception:
                continue
        # Form pairs
        pairs = [[self.normalizeString(s) for s in l.split('\t')] 
                            for l in self.convert2turns(lines) if '\t' in l]
        return pairs

    def readQA(self,dataPath):
        # Read QA file
        lines = []
        for line in open(dataPath,'r').readlines()[:1000]:
            try:
                fields = line.split('\t')
                question = fields[0].strip()
                answer = fields[1].strip()
                if '0' in fields[2].strip(): continue
                lines.append( [self.normalizeString(question),
                               self.normalizeString(answer)] )
            except Exception:
                print('Exception!')
                continue
        # Form pairs
        pairs = lines
        return pairs

    def readData(self, source, target, reverse=False):
        print("Reading lines...")

        # Read different datasets
        if 'movie' in dataPath:
            pairs = self.readMovies(dataPath)
        else:
            pairs = self.readQA(dataPath)

        # Reverse pairs, make Vocab instances
        if reverse:
            pairs = [list(reversed(p)) for p in pairs]
            input_source = Vocab(target)
            output_target = Vocab(source)
        else:
            input_source = Vocab(source)
            output_target = Vocab(target)

        return input_source, output_target, pairs

    def unicodeToAscii(self,s):
        return ''.join(
            c for c in unicodedata.normalize('NFD', s)
            if unicodedata.category(c) != 'Mn'
        )

    # Lowercase, trim, and remove non-letter characters
    def normalizeString(self,s):
        s = self.unicodeToAscii(s.lower().strip())
        s = re.sub(r"([.!?])", r" \1", s)
        s = re.sub(r"[^a-zA-Z.!?]+", r" ", s)
        return s

    def pad_indices(self,max_length,original):
        padded_z = Variable(torch.ones(max_length, 1)) # init with 1 for EOS
        for ei in range(max_length):
            if ei>len(original)-1: break
            padded_z[ei] = original[ei]
        return padded_z

class Vocab:
    def __init__(self, name):
        self.name = name
        self.word2index = {'UNK': 2}
        self.word2count = {}
        self.index2word = {0: "SOS", 1: "EOS", 2: "UNK"}
        self.n_words = 3  
        self.max_len = 0

    def addSentence(self, sentence):
        for word in sentence.split(' '):
            self.addWord(word)

    def addWord(self, word):
        if word not in self.word2index:
            self.word2index[word] = self.n_words
            self.word2count[word] = 1
            self.index2word[self.n_words] = word
            self.n_words += 1
        else:
            self.word2count[word] += 1


def compat_splitting(line):
    return line.decode('utf8').split()

# Load pretrained embedding
def loadEmb(path='embedding/model.vec'):

    # Load from file
    vectors = {}
    fin = open(path, 'rb')
    for i, line in enumerate(fin):
        if i==0:
            tab = compat_splitting(line)
            num_embeddings = int(tab[0])+1
            embedding_dim = int(tab[1])
            continue
        try:
            tab = compat_splitting(line)
            vec = [float(n) for n in tab[1:]]
            word = tab[0]
            if not word in vectors:
                vectors[word] = vec
        except ValueError:
            continue
        except UnicodeDecodeError:
            continue
    fin.close()

    # Construct embedding matrix
    import torch.nn as nn
    pretrained_embeddings_matrix = [] # idx x vec
    stoi = {'UNK': 0} # token -> idx

    for idx,token in enumerate(vectors.keys()):
        pretrained_embeddings_matrix.append( vectors[token] )
        stoi[token] = idx
    
    pretrained_embeddings_matrix = torch.FloatTensor( [[0.]*embedding_dim] + pretrained_embeddings_matrix)

    padding_idx = 0
    embedding = nn.Embedding(num_embeddings,
                             embedding_dim,
                             padding_idx)
    embedding.weight.data.copy_( pretrained_embeddings_matrix )

    return embedding,pretrained_embeddings_matrix,stoi

# For testing 
if __name__ == "__main__":

    embedding,pretrained_embeddings_matrix,stoi = loadEmb()












