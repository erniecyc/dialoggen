# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Title generator is a system that generates titles.')

# Data and loading options
parser.add_argument('-data', required=False,
                             help='Path to the data.',
                             default='rescoring/')
parser.add_argument('-model', required=False,
                             help='Path to save model.',
                             default='models')
parser.add_argument('-adv', action='store_true',
                             help='Specify if adversarial training is to be persued.')
parser.add_argument('-epochs', required=False,
                             help='Maximum number of epochs.',
                             type=int,
                             default=1000)
parser.add_argument('-patience', required=False,
                             help='Number of epochs to wait.',
                             type=int,
                             default=5)
parser.add_argument('-print_every', required=False,
                             help='Number of epochs to print.',
                             type=int,
                             default=100)
parser.add_argument('-lr', required=False,
                             help='Learning rate.',
                             type=int,
                             default=0.0001)
parser.add_argument('-start_checkpoint_at', required=False,
                             help='Number of epochs to start saving checkpoints.',
                             type=int,
                             default=3)
parser.add_argument('-max_len', required=False,
                             help='Maximum sentence length.',
                             type=int,
                             default=150)
parser.add_argument('-batch', required=False,
                             help='Batch size.',
                             type=int,
                             default=32)
parser.add_argument('-rnnlayers', required=False,
                             help='Number of RNN layers.',
                             type=int,
                             default=1)
parser.add_argument('-resume', action='store_true',
                             help='To resume from previous checkpoint or not.')
parser.add_argument('-checkpoint', required=False,
                             help='Specify checkpoint file path.',
                             default='model_9')



options = parser.parse_args()




